package com.example.kostya_m.seabattle.di.modules

import com.example.kostya_m.seabattle.data.repo.GameFieldRepo
import com.example.kostya_m.seabattle.di.PerActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import javax.inject.Named

/**
 * Created by Kostya_M on 16.11.2016.
 */
@Module
class GameModule {
    @Provides
    @PerActivity
    @Named("MyFieldRepo")
    fun provideMyFieldRepo() : GameFieldRepo {
        return GameFieldRepo()
    }

    @Provides
    @PerActivity
    @Named("EnemyFieldRepo")
    fun provideEnemyFieldRepo() : GameFieldRepo {
        return GameFieldRepo()
    }
}