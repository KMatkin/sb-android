package com.example.kostya_m.seabattle.di

import javax.inject.Qualifier

/**
 * Created by Kostya_M on 26.10.2016.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext