package com.example.kostya_m.seabattle.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TableLayout
import android.widget.TableRow
import com.example.kostya_m.seabattle.R

/**
 * Created by Kostya_M on 29.10.2016.
 */
class FieldLayout: TableLayout {
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val arr = context.obtainStyledAttributes(attrs, R.styleable.FieldLayout)
        val size = arr.getInteger(R.styleable.FieldLayout_size, 1)
        for(i in 1..size) {
            val row = TableRow(context, attrs)
            row.weightSum = 10f
            for(j in 1..size) {
                val cell = FieldCellView(context)
                row.addView(cell)
            }
            this.addView(row)
        }
        arr.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = View.getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val width = View.getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val size = Math.min(height, width)
        setMeasuredDimension(size, size)
    }
}