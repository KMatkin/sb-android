package com.example.kostya_m.seabattle.data.service

import com.example.kostya_m.seabattle.data.model.remote.*
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*
import rx.Observable
import rx.Single

/**
 * Created by Kostya_M on 29.10.2016.
 */
interface SeaBattleApi {
    /*
     * USERS
     */
    @POST("player")
    fun createUser() : Single<Session>


    /*
     * ROOMS
     */
    @POST("room")
    fun createRoom(@Body session: Session) : Single<Int>

    @GET("room/list")
    fun getRoomList() : Single<List<Room>>

    @POST("room/{id}/join")
    fun joinRoom(@Path("id") roomId: Int, @Body session: Session) : Single<ResponseBody>

    @POST("room/{id}/ready")
    fun ready(@Path("id") roomId: Int, @Body session: Session) : Single<ResponseBody>

    @POST("room/{id}/leave")
    fun leaveRoom(@Path("id") roomId: Int, @Body session: Session) : Single<ResponseBody>

    @POST("room/{id}/unready")
    fun unready(@Path("id") roomId: Int, @Body session: Session) : Single<ResponseBody>

    @GET("room/{id}/updates/{token}/{securityToken}")
    fun roomInfo(@Path("id") roomId: Int, @Path("token") token: String,
                 @Path("securityToken") securityToken: String) : Observable<GameReadyUpdate>

    @GET("room/{id}/playerUpdates")
    fun playerUpdate(@Path("id") roomId: Int) : Observable<PlayersCountChangedUpdate>

    /*
     * GAME
     */
    @POST("game/setShips")
    fun setShips(@Body request: ShipsRequest) : Single<ResponseBody>

    @GET("game/updates/{gameToken}/{token}/{securityToken}")
    fun gameInfo(@Path("gameToken") gameToken: String, @Path("token") userToken: String,
                 @Path("securityToken") userSecurityToken: String) : Observable<GameInfoUpdate>

    @POST("game/move")
    fun makeStep(@Body request: MakeStepRequest) : Single<ResponseBody>
}