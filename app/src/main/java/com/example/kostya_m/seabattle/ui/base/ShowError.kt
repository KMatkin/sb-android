package com.example.kostya_m.seabattle.ui.base

/**
 * Created by Kostya_M on 19.11.2016.
 */
interface ShowError {
    fun showError(th: Throwable)
}