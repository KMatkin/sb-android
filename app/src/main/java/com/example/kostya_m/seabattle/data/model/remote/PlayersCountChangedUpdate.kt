package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 03.12.2016.
 */
data class PlayersCountChangedUpdate(val roomId: Int, val playersCount: Int, val type: Int)