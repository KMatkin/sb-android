package com.example.kostya_m.seabattle.ui.base

/**
 * Created by Kostya_M on 23.10.2016.
 */
open class BaseMvpPresenter<T: MvpView> : MvpPresenter<T> {
    private var _view: T? = null
    val view: T
        get() { return _view ?: throw MvpViewNotAttachedException() }

    override fun attachView(view: T) {
        _view = view
    }

    override fun detachView() {
        _view = null
    }

    class MvpViewNotAttachedException : RuntimeException(
            "Please call Presenter.attachView(MvpView) before requesting data to the Presenter")
}