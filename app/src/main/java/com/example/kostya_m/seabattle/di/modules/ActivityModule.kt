package com.example.kostya_m.seabattle.di.modules

import android.app.Activity
import android.content.Context
import com.example.kostya_m.seabattle.di.ActivityContext
import com.example.kostya_m.seabattle.di.ApplicationContext
import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.util.navigation.Navigator
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Kostya_M on 23.10.2016.
 */
@Module
class ActivityModule (private val activity: Activity) {

    @Provides
    @PerActivity
    internal fun provideActivity() : Activity {
        return activity
    }

    @Provides
    @PerActivity
    @ActivityContext
    internal fun providesContext() : Context {
        return activity
    }
}