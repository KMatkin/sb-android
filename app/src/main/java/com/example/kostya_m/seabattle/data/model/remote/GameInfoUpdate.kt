package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 16.11.2016.
 */
data class GameInfoUpdate(val gameInfo: GameInfo)