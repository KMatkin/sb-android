package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 20.11.2016.
 */
object CellTypes {
    val Empty = 0
    val Ship = 1
}