package com.example.kostya_m.seabattle.data.model.remote

import com.example.kostya_m.seabattle.data.model.local.CellType

/**
 * Created by Kostya_M on 20.11.2016.
 */
data class Cell(val type: Int, val isHit: Boolean, val x: Int, val y: Int, val isDestroyed: Boolean) {
    fun getConvertedCellType(myCell: Boolean) : CellType {
        return when(type) {
            CellTypes.Empty -> if (isHit) CellType.MISS else CellType.EMPTY
            CellTypes.Ship -> when(Pair(myCell, isHit)) {
                    Pair(true, true) -> if (!isDestroyed) CellType.HIT_ME else CellType.DESTROYED_ME
                    Pair(true, false) -> CellType.FILL
                    Pair(false, true) -> if (!isDestroyed) CellType.HIT_ENEMY else CellType.DESTROYED_ENEMY
                    else -> CellType.EMPTY// TODO проверить
                }
            else -> {
                throw Exception("Unknown cell type")
            }
        }
    }
}