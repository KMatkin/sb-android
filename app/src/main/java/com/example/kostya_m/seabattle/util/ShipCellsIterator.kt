package com.example.kostya_m.seabattle.util

import com.example.kostya_m.seabattle.data.model.local.ShipOrientation

/**
 * Created by Kostya_M on 30.10.2016.
 */
class ShipCellsIterator(val x: Int, val y: Int, val shipLength: Int, val orientation: ShipOrientation):
        AbstractIterator<Pair<Int, Int>>() {
    private val dx : Int
    private val dy : Int
    private var i = 0

    init {
        if (orientation == ShipOrientation.HORIZONTAL) {
            dx = 1
            dy = 0
        } else {
            dx = 0
            dy = 1
        }
    }

    override fun computeNext() {
        if (i == shipLength)
            done()
        else {
            setNext(Pair(x + i*dx, y + i*dy))
            i++
        }
    }
}

fun shipCells(x: Int, y: Int, length: Int, orientation: ShipOrientation, func: (Int, Int) -> Unit) {
    for (point in ShipCellsIterator(x, y, length, orientation)) {
        func(point.first, point.second)
    }
}