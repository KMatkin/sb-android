package com.example.kostya_m.seabattle.data.service

import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.remote.GameInfoUpdate
import com.example.kostya_m.seabattle.data.model.remote.MakeStepRequest
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.model.remote.ShipsRequest
import com.example.kostya_m.seabattle.di.ConfigPersistent
import rx.Observable
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Kostya_M on 20.11.2016.
 */
@ConfigPersistent
class GameService
@Inject constructor(private val api: SeaBattleApi) {

    fun setShips(gameToken: String, ships: List<Ship>, session: Session) : Single<Any> {
        val remoteShips = ships.map {
            com.example.kostya_m.seabattle.data.model.remote.Ship.valueOf(it)
        }
        return api.setShips(ShipsRequest(gameToken, remoteShips, session.token, session.securityToken))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { Any() }
    }

    fun getGameUpdates(gameToken: String, currentSession: Session) : Observable<GameInfoUpdate> =
        api.gameInfo(gameToken, currentSession.token, currentSession.securityToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

    fun makeStep(gameToken: String, session: Session, x: Int, y: Int) =
        api.makeStep(MakeStepRequest(gameToken, session.token, session.securityToken, x, y))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { Any() }
}