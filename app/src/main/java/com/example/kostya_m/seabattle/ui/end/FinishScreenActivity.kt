package com.example.kostya_m.seabattle.ui.end

import android.os.Bundle
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_end.*

/**
 * Created by Kostya_M on 02.12.2016.
 */
class FinishScreenActivity : BaseActivity() {
    companion object {
        @JvmStatic
        val ARG_YOU_WON = "YOU_WON"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_end)
        if (intent.extras.getBoolean(ARG_YOU_WON)) {
            endResultText.setText(R.string.you_won)
        } else {
            endResultText.setText(R.string.you_lose)
        }

        endRetryButton.setOnClickListener {
            finish()
        }
    }
}