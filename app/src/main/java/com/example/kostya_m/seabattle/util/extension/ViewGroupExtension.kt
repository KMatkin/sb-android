package com.example.kostya_m.seabattle.util.extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Kostya_M on 30.10.2016.
 */


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}