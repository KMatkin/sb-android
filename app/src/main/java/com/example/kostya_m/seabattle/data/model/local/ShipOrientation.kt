package com.example.kostya_m.seabattle.data.model.local

/**
 * Created by Kostya_M on 30.10.2016.
 */
enum class ShipOrientation {
    HORIZONTAL,
    VERTICAL
}