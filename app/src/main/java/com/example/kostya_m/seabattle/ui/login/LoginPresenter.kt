package com.example.kostya_m.seabattle.ui.login

import com.example.kostya_m.seabattle.di.ConfigPersistent
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

/**
 * Created by Kostya_M on 26.10.2016.
 */
@ConfigPersistent
class LoginPresenter
@Inject
constructor() : LoginContract.Presenter() {
    private val compositeSubscription = CompositeSubscription()

    override fun detachView() {
        super.detachView()
        compositeSubscription.clear()
    }
}