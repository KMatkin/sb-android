package com.example.kostya_m.seabattle.ui.game

import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.ui.base.BaseMvpPresenter
import com.example.kostya_m.seabattle.ui.base.MvpPresenter
import com.example.kostya_m.seabattle.ui.base.MvpView
import rx.Observable

/**
 * Created by Kostya_M on 15.11.2016.
 */
object GameContract {
    interface View : MvpView {
        fun showMyField(cells: Array<CellType>)
        fun showEnemyCell(cellType: CellType, x: Int, y: Int)
        fun showMyCell(cellType: CellType, x: Int, y: Int)
        fun showStepMessage(myTurn: Boolean)
        fun gameFinished(youWon: Boolean)
    }

    abstract class Presenter : BaseMvpPresenter<View>() {
        abstract fun makeStep(x: Int, y:Int)
        abstract fun requestMyField()
        abstract fun subscribeToGameUpdates()
        abstract fun unsubscribeToGameUpdates()
    }
}