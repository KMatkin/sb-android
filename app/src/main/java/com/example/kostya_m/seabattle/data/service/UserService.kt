package com.example.kostya_m.seabattle.data.service

import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.di.ConfigPersistent
import rx.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Kostya_M on 19.11.2016.
 */
@Singleton
class UserService
@Inject constructor(private val api: SeaBattleApi) {
    fun createUser() : Single<Session> {
        return api.createUser()
    }
}