package com.example.kostya_m.seabattle.di

import javax.inject.Scope

/**
 * Created by Kostya_M on 26.10.2016.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ConfigPersistent