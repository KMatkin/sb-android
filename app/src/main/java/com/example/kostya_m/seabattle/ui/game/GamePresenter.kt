package com.example.kostya_m.seabattle.ui.game

import com.example.kostya_m.seabattle.data.manager.GameManager
import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.data.model.remote.Cell
import com.example.kostya_m.seabattle.data.model.remote.GameStates
import com.example.kostya_m.seabattle.di.ConfigPersistent
import rx.Subscription
import rx.lang.kotlin.deferredObservable
import timber.log.Timber
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Kostya_M on 16.11.2016.
 */
@ConfigPersistent
class GamePresenter
@Inject constructor(val gameManager: GameManager): GameContract.Presenter() {

    var gameToken : String? = null

    var updateSubscription : Subscription? = null

    var lastStepWasMine : Boolean? = null

    override fun makeStep(x: Int, y: Int) {
        gameManager.makeStep(x, y)
                .subscribe({
                    Timber.d("Successfully make step in %d %d", x, y)
                    // Проверка
                    // view.showEnemyCell()
                }, { e ->

                })
    }

    override fun requestMyField() {
        gameManager.getMyField()
                .subscribe {
                    view.showMyField(it)
                }
    }

    override fun subscribeToGameUpdates() {
        updateSubscription = gameManager.gameUpdates()
                .timeout(30, TimeUnit.SECONDS)
                .retryWhen {
                    errors -> errors.filter {
                        it is SocketTimeoutException
                    }
                }
                .repeat()
                .subscribe({
                    if (it?.gameInfo!!.winner != null) {
                        view.gameFinished(it.gameInfo.winner?.token == gameManager.session.get().token)
                    } else {
                        // Turn
                        view.showStepMessage(it?.gameInfo!!.isMyTurn)
                        lastStepWasMine = it?.gameInfo.isMyTurn
                        val myChangedCells = gameManager.updateMyField(it.
                                gameInfo.myField)
                        showMyCells(myChangedCells)
                        val enemyChangedCells = gameManager.updateEnemyField(it.gameInfo.opponentField)
                        showEnemyCells(enemyChangedCells)
                    }
                }, {
                    e-> Timber.e("Cannot get game update")
                    e.printStackTrace()
                })
    }

    override fun unsubscribeToGameUpdates() {
        updateSubscription?.unsubscribe()
    }

    private fun showMyCells(cells: List<Cell>) {
        showCells(cells, true)
    }

    private fun showEnemyCells(cells: List<Cell>) {
        showCells(cells, false)
    }

    private fun showCells( cells: List<Cell>, me: Boolean) {
        cells.forEach {
            if (me) {
                view.showMyCell(it.getConvertedCellType(me), it.x, it.y)
            } else {
                view.showEnemyCell(it.getConvertedCellType(me), it.x, it.y)
            }
        }
    }
}