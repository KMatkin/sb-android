package com.example.kostya_m.seabattle.di.components

import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.di.modules.ActivityModule
import com.example.kostya_m.seabattle.di.modules.GameModule
import com.example.kostya_m.seabattle.ui.arrange.ArrangeActivity
import com.example.kostya_m.seabattle.ui.game.GameActivity
import dagger.Component
import dagger.Subcomponent

/**
 * Created by Kostya_M on 16.11.2016.
 */
@PerActivity
@Subcomponent(modules = arrayOf(GameModule::class))
interface GameComponent {
    fun inject(gameActivity: GameActivity)
    fun inject(arrangeActivity: ArrangeActivity)
}