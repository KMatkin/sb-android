package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 17.11.2016.
 */
data class Session(var token: String, var securityToken: String)