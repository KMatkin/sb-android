package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 20.11.2016.
 */
data class GameInfo(var state: Int, var isMyTurn : Boolean,
                    var myField: List<List<Cell>>, var opponentField: List<List<Cell>>,
                    var winner: Session?)