package com.example.kostya_m.seabattle.ui.room

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.di.ApplicationContext
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.extension.getCompatColor
import com.example.kostya_m.seabattle.util.extension.inflate
import javax.inject.Inject

/**
 * Created by Kostya_M on 19.11.2016.
 */
@ConfigPersistent
class RoomAdapter
@Inject constructor(@ApplicationContext val context: Context) : RecyclerView.Adapter<RoomAdapter.RoomViewHolder>() {
    var items : MutableList<Room>? = null
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (items != null) items!!.size else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
        return RoomViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RoomViewHolder?, position: Int) {
        holder?.bind(items!![position])
    }

    inner class RoomViewHolder(parent : ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.room_item)) {
        fun bind(room: Room) {
            val roomTextView = itemView.findViewById(R.id.roomItemText) as TextView
            roomTextView.text = String.format(context.getString(R.string.room_name_format), room.id.toString())
            val playersTextView = itemView.findViewById(R.id.roomPlayers) as TextView
            playersTextView.text = String.format(context.getString(R.string.room_players_format), room.players.size)
            val circle = playersTextView.background as GradientDrawable
            circle.setColor(if (room.isFull())  context.getCompatColor(R.color.filledRoom)
                else context.getCompatColor(R.color.notFilledRoom))
        }
    }
}