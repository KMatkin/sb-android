package com.example.kostya_m.seabattle.ui.room

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.di.PerActivity
import kotlinx.android.synthetic.main.activity_room.*
import kotlinx.android.synthetic.main.fragment_room_info.*
import javax.inject.Inject

/**
 * Created by Kostya_M on 19.11.2016.
 */
@PerActivity
class RoomInfoFragment
@Inject constructor(val room: Room) : Fragment() {
    @Inject lateinit var presenter : RoomPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_room_info, container, false)
        val roomActivity = activity as RoomActivity
        roomActivity.toolbar.setNavigationOnClickListener {
            activity.onBackPressed()
        }
        roomActivity.roomComponent?.inject(this)
        return view
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribeToRoomUpdates(room)
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribeToRoomUpdates()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribeToRoomUpdates()
        presenter.leaveRoom(room)
    }

    fun setReady(value: Boolean) {
        presenter.requestSetReady(room)
        presenter.view.setReady(value)
        if (value) {
            roomInfoText.text = "Ожидание готовности противника"
            roomInfoProgressBar.visibility = View.VISIBLE
//            presenter.subscribeToRoomUpdates(room)
        } else {
            roomInfoText.text = "Нажмите готов"
            roomInfoProgressBar.visibility = View.INVISIBLE
//            presenter.unsubscribeToRoomUpdates()
        }
    }

    fun fabClicked() {
        setReady(!(presenter.getReady() ?: false))
    }
}