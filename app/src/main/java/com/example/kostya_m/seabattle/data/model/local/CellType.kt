package com.example.kostya_m.seabattle.data.model.local

/**
 * Created by Kostya_M on 29.10.2016.
 */
enum class CellType(val value: Int) {
    EMPTY(0),
    MISS(1),
    HIT_ME(2),
    HIT_ENEMY(3),
    FILL(4),
    DESTROYED_ME(5),
    DESTROYED_ENEMY(6)
}