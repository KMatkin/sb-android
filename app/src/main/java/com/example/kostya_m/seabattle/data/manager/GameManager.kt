package com.example.kostya_m.seabattle.data.manager

import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.remote.Cell
import com.example.kostya_m.seabattle.data.model.remote.GameInfoUpdate
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.repo.GameFieldRepo
import com.example.kostya_m.seabattle.data.service.GameService
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.shipCells
import rx.Observable
import rx.Single
import rx.lang.kotlin.emptyObservable
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Kostya_M on 16.11.2016.
 */
@ConfigPersistent
class GameManager
@Inject
constructor(private val gameService: GameService, val session: dagger.Lazy<Session>) {

    var gameToken : String? = null

    val enemyFieldRepo: GameFieldRepo = GameFieldRepo()

    val myFieldRepo: GameFieldRepo = GameFieldRepo()

    fun getMyField() : Observable<Array<CellType>> = TODO() // TODO: Запрос к серверу

    fun makeStep(x: Int, y: Int) : Observable<Any> {
        if (enemyFieldRepo.getCell(x, y) == CellType.EMPTY) {
            return gameService.makeStep(gameToken!!, session.get(), x, y).toObservable()
        } else {
            return emptyObservable()
        }
    }

    fun gameUpdates() : Observable<GameInfoUpdate> {
        return gameService.getGameUpdates(gameToken!!, session.get())
    }

    fun updateMyField(field: List<List<Cell>>) : List<Cell> {
        return updateField(myFieldRepo, field, true)

    }

    fun updateEnemyField(field: List<List<Cell>>) : List<Cell> {
        return updateField(enemyFieldRepo, field, false)
    }

    private fun updateField(fieldRepo: GameFieldRepo, field: List<List<Cell>>, me: Boolean) : List<Cell> {
        val res = mutableListOf<Cell>()
        for (row in field) {
            for (cell in row) {
                // Changed type
                if (cell.getConvertedCellType(me) != fieldRepo.getCell(cell.x, cell.y)) {
                    res.add(cell)
                    fieldRepo.setCell(cell.x, cell.y, cell.getConvertedCellType(me))
                }
            }
        }
        return res
    }

    fun setMyShips(ships: List<Ship>) {
        ships.forEach { ship ->
            shipCells(ship.x!!, ship.y!!, ship.length, ship.orientation, {
                x, y -> myFieldRepo.setCell(x, y, CellType.FILL)
            })
        }
    }
}