package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 17.11.2016.
 */
data class Room(val id: Int, val owner: Session, val players: List<Session>) {
    fun isFull() = players.size == 2
    fun playerCount() = players.size
}