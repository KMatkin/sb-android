package com.example.kostya_m.seabattle.data.repo

import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.di.ConfigPersistent
import javax.inject.Inject

/**
 * Created by Kostya_M on 16.11.2016.
 */
@ConfigPersistent
class GameFieldRepo @Inject constructor() {
    companion object {
        val DEFAULT_SIZE = 10
    }

    var field = Array(DEFAULT_SIZE * DEFAULT_SIZE, {CellType.EMPTY})

    fun getCell(x: Int, y: Int) : CellType = field[y * DEFAULT_SIZE + x]

    fun setCell(x: Int, y: Int, cellType: CellType) {
        field[y * DEFAULT_SIZE + x] = cellType
    }
}