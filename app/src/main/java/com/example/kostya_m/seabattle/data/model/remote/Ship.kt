package com.example.kostya_m.seabattle.data.model.remote

import com.example.kostya_m.seabattle.data.*
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation

/**
 * Created by Kostya_M on 20.11.2016.
 */
data class Ship(val isVertical: Boolean, val size: Int, val x: Int, val y: Int) {
    companion object {
        fun valueOf(ship: com.example.kostya_m.seabattle.data.model.local.Ship) : Ship {
            return Ship(isVertical = ship.orientation == ShipOrientation.VERTICAL,
                        size = ship.length,
                        x = ship.x!!,
                        y = ship.y!!)
        }
    }
}

