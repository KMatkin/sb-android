package com.example.kostya_m.seabattle.di.components

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.service.SeaBattleApi
import com.example.kostya_m.seabattle.di.modules.ApplicationModule
import com.example.kostya_m.seabattle.ui.base.BaseActivity
import com.example.kostya_m.seabattle.di.ApplicationContext
import com.example.kostya_m.seabattle.di.modules.DataModule
import dagger.Component
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Kostya_M on 23.10.2016.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, DataModule::class))
interface ApplicationComponent {

    @ApplicationContext fun context() : Context
    fun application(): SeaBattleApplication
    fun seaBattleApi() : SeaBattleApi
    fun sharedPreferences() : SharedPreferences
    fun currentSession() : Session
}