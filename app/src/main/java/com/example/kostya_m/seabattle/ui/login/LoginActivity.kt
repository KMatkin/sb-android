package com.example.kostya_m.seabattle.ui.login

import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.ui.base.BaseActivity
import org.jetbrains.anko.*
import javax.inject.Inject

class LoginActivity: BaseActivity(), LoginContract.View {

    @Inject
    lateinit var presenter: LoginPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LoginActivityUI().setContentView(this)
        activityComponent.inject(this)
        presenter.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    inner class LoginActivityUI: AnkoComponent<LoginActivity> {
        override fun createView(ui: AnkoContext<LoginActivity>): View = with(ui) {
            return frameLayout {
                verticalLayout {
                    verticalLayout {
                        gravity = Gravity.CENTER
                        textView(text = "Логин") {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }
                        editText {

                        }
                        textView(text = "Пароль") {
                            gravity = Gravity.CENTER_HORIZONTAL
                        }
                        editText {
                            inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
                        }
                    }
                    verticalLayout {
                        gravity = Gravity.BOTTOM
                        button(text = "Войти") {
                        }
                        button(text = "Зарегестрироваться") {
                        }
                    }

                }.apply {
                    layoutParams = FrameLayout.LayoutParams(matchParent, matchParent)
                            .apply { padding = dip(16) }
                }
                include<FrameLayout>(R.layout.progress_view)
            }
        }
    }
}
