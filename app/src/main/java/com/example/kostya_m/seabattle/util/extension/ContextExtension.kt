package com.example.kostya_m.seabattle.util.extension

import android.accounts.NetworkErrorException
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.di.components.ApplicationComponent
import com.example.kostya_m.seabattle.util.exceptions.RoomFullException
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.toast
import timber.log.Timber
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

/**
 * Created by Kostya_M on 26.10.2016.
 */

fun Context.getApplicationComponent(): ApplicationComponent {
    return (applicationContext as SeaBattleApplication).applicationComponent
}

fun Context.isNetworkConnected(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return cm.activeNetworkInfo?.isConnectedOrConnecting ?: false
}

fun Context.getCompatColor(@ColorInt colorRes: Int) : Int {
    return ContextCompat.getColor(this, colorRes)
}

fun Context.getCompatDrawable(@DrawableRes drawableRes: Int) : Drawable {
    return ContextCompat.getDrawable(this, drawableRes)
}

fun Context.showErrorToast(th: Throwable) {
    runOnUiThread {
        when(th) {
            is NetworkErrorException, is UnknownHostException
            -> toast(getString(R.string.message_network_error))
            is TimeoutException -> toast(getString(R.string.message_timeout_error))
            is RoomFullException -> toast(getString(R.string.message_room_full))
            else -> toast(th.message ?: "Неизвестная ошибка")
        }
        Timber.e(th)
    }
}