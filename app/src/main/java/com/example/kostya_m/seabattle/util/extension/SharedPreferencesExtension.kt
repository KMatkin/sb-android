package com.example.kostya_m.seabattle.util.extension

import android.content.SharedPreferences

/**
 * Created by Kostya_M on 20.11.2016.
 */

object PREF_KEYS {
    val PREF_GAME = "GAME_TOKEN"
}

fun SharedPreferences.putString(key: String, value: String) {
    with (this.edit()) {
        putString(key, value)
        apply()
    }
}

fun SharedPreferences.getString(key: String) : String {
    return this.getString(key, "")
}