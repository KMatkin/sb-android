package com.example.kostya_m.seabattle.data.manager

import com.example.kostya_m.seabattle.data.model.remote.GameReadyUpdate
import com.example.kostya_m.seabattle.data.model.remote.PlayersCountChangedUpdate
import com.example.kostya_m.seabattle.data.model.remote.Room
import rx.Observable
import rx.Single

/**
 * Created by Kostya_M on 19.11.2016.
 */
abstract class RoomManager {
    protected var meReady : Boolean? = null
    var currentRoom : Room? = null

    abstract fun getRoomList() : Single<List<Room>>

    open fun getReady() = meReady

    open fun setReady(room: Room): Single<Room> {
        meReady = meReady?.not() ?: true
        return Single.just(room)
    }

    abstract fun joinRoom(room: Room) : Single<Room>

    abstract fun leaveRoom(room: Room) : Single<Room>

    abstract fun createRoom() : Single<Room>

    abstract fun updateRoom(room: Room) : Observable<GameReadyUpdate>

    abstract fun getRoom(room: Room) : Single<Room>
    abstract fun updatePlayers(room: Room): Observable<PlayersCountChangedUpdate>
}