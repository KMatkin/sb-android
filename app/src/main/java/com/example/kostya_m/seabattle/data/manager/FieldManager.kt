package com.example.kostya_m.seabattle.data.manager

import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.di.PerActivity

/**
 * Created by Kostya_M on 29.10.2016.
 */
@PerActivity
@ConfigPersistent
class FieldManager