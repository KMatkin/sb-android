package com.example.kostya_m.seabattle.di.modules

import android.app.Application
import android.content.Context
import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.di.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Kostya_M on 23.10.2016.
 */
@Module
class ApplicationModule (private val application: SeaBattleApplication) {

    @Provides
    @Singleton
    internal fun provideApplication(): SeaBattleApplication {
        return application
    }

    @Provides
    @Singleton
    @ApplicationContext
    internal fun provideContext() : Context {
        return application
    }

    @Provides
    @Singleton
    @Named("CurrentSession")
    fun provideCurrentSession() : Session {
        return Session("", "")
    }
}