package com.example.kostya_m.seabattle.ui.widgets

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.view.GestureDetectorCompat
import android.support.v4.view.MotionEventCompat
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.util.extension.getCompatColor
import com.example.kostya_m.seabattle.util.extension.isZeroFraction
import org.jetbrains.anko.dip
import org.jetbrains.anko.onClick
import timber.log.Timber

/**
 * Created by Kostya_M on 30.10.2016.
 */
class GameFieldView : View {
    private lateinit var gameBitmap: Bitmap
    val horizontalCellsCount = 10
    val verticalCellsCount = 10
    var viewSize: Int = dip(300)
    private var cellWidth: Int = 0
    private var cellHeight: Int = 0
    private lateinit var canvas: Canvas
    private var paint: Paint
    private var borderColor = context.getCompatColor(R.color.gridColor)
    private var strokeWidth = 1f
    private var gestureDetector: GestureDetectorCompat
    private var longPressGestureListener: LongPressGestureListener
    var longPressListener : LongPressListener? = null
    var singleTapListener : SingleTapListener? = null
    private var measured: Boolean = false
    var initCells : List<Pair<Pair<Int, Int>, CellType>>? = null

    init {
        paint = Paint()
        paint.isAntiAlias = true
        paint.isDither = true
        paint.color = borderColor
        paint.strokeWidth = strokeWidth
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeCap = Paint.Cap.ROUND
        longPressGestureListener = LongPressGestureListener()
        gestureDetector = GestureDetectorCompat(context, longPressGestureListener)
    }

    constructor(context: Context): super(context) {

    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

    }

    fun fillCell(cellX: Int, cellY: Int, cellType: CellType) {
        val top: Float = (cellY * cellHeight + (cellY+1) * strokeWidth)
        val left: Float = (cellX * cellWidth + (cellX+1) * strokeWidth)
        val right: Float = left + cellWidth
        val bottom: Float = top + cellHeight
        var color: Int = Color.WHITE
        var style: Paint.Style = Paint.Style.FILL
        when (cellType) {
            CellType.EMPTY -> color = context.getCompatColor(R.color.emptyColor)
            CellType.FILL -> color = context.getCompatColor(R.color.allianceColor)
            CellType.HIT_ME -> color = context.getCompatColor(R.color.shipBorderColor)
            CellType.MISS -> color = context.getCompatColor(R.color.borderColor)
            CellType.HIT_ENEMY -> color = context.getCompatColor(R.color.enemyColor)
            CellType.DESTROYED_ME -> color = context.getCompatColor(R.color.shipBorderColor)
            CellType.DESTROYED_ENEMY -> color = context.getCompatColor(R.color.enemyColor)
        }
        paint.color = color
        paint.style = style
        when (cellType) {
            CellType.EMPTY, CellType.FILL ->  {
                canvas.drawRect(left, top, right, bottom, paint)
            }
            CellType.MISS -> {
                paint.style = Paint.Style.STROKE
                canvas.drawRect(left, top, right, bottom, paint)
                paint.style = style
                canvas.drawCircle((left + right) / 2, (top + bottom) / 2, cellHeight / 6.0f, paint)
            }
            CellType.HIT_ME -> {
                paint.style = Paint.Style.STROKE
                // Draw border
                canvas.drawRect(left, top, right, bottom, paint)
                // Draw X
                paint.strokeWidth = 2f
                canvas.drawLine(left, top, right, bottom, paint)
                canvas.drawLine(left, bottom, right, top, paint)
                paint.strokeWidth = strokeWidth
            }
            CellType.HIT_ENEMY -> {
                canvas.drawRect(left, top, right, bottom, paint)
                paint.style = Paint.Style.STROKE
                paint.color = context.getCompatColor(R.color.shipBorderColor)
                // Draw border
                canvas.drawRect(left, top, right, bottom, paint)
                paint.strokeWidth = 2f
                // Draw X
                canvas.drawLine(left, top, right, bottom, paint)
                canvas.drawLine(left, bottom, right, top, paint)
                paint.strokeWidth = strokeWidth
            }
            CellType.DESTROYED_ME -> {
                // Draw X
                canvas.drawLine(left, top, right, bottom, paint)
                canvas.drawLine(left, bottom, right, top, paint)
                paint.strokeWidth = strokeWidth

                paint.style = Paint.Style.STROKE
                paint.strokeWidth = 4f
                // Border
                canvas.drawRect(left-2, top, right-2, bottom-2, paint)
                paint.strokeWidth = 2f
            }
            CellType.DESTROYED_ENEMY -> {
                canvas.drawRect(left, top, right, bottom, paint)
                paint.style = Paint.Style.STROKE
                paint.strokeWidth = 4f
                paint.color = context.getCompatColor(R.color.shipBorderColor)
                // Draw border
                canvas.drawRect(left, top, right, bottom, paint)
                paint.strokeWidth = 2f
                // Draw X
                canvas.drawLine(left, top, right, bottom, paint)
                canvas.drawLine(left, bottom, right, top, paint)
                paint.strokeWidth = strokeWidth
            }
        }
        Timber.d("CellX= %d, CellY=%d", cellX, cellY)
        invalidate()
    }

    fun convertToRelative(x: Int, y:Int): Pair<Int, Int> {
        return Pair((x.toFloat() / (cellWidth + strokeWidth)).toInt(),
                (y.toFloat() / (cellHeight + strokeWidth)).toInt())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.save()
        if (initCells != null) {
            drawField()
            initCells = null
        }
        canvas.drawBitmap(gameBitmap, 0f, 0f, paint)
        canvas.restore()
    }

    private fun drawField() {
        initCells?.forEach {
            fillCell(it.first.first, it.first.second, it.second)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (measured) {
            setMeasuredDimension(viewSize, viewSize)
            return
        }

        val height = View.getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val width = View.getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        var size = Math.min(height, width)
        val widthForLines = (strokeWidth * (horizontalCellsCount + 1))
        if (!(size - widthForLines / horizontalCellsCount).isZeroFraction()) {
            val remainingWidth = (size - widthForLines).toInt()
            val cellWidth = remainingWidth / horizontalCellsCount
            size = (cellWidth * horizontalCellsCount + widthForLines).toInt()
        }

        this.viewSize = size
        gameBitmap = Bitmap.createBitmap(viewSize, viewSize, Bitmap.Config.ARGB_8888)
        canvas = Canvas(gameBitmap)
        canvas.drawColor(context.getCompatColor(R.color.backgroundColor))


        cellWidth = ((viewSize - strokeWidth * (horizontalCellsCount + 1)) / horizontalCellsCount).toInt()
        cellHeight = cellWidth

        drawGrid()
        setMeasuredDimension(size, size)
        measured = true
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        gestureDetector.onTouchEvent(event)
        return true
    }

    fun drawGrid() {
        for (x in 0..horizontalCellsCount + 1) {
            canvas.drawLine(x.toFloat() * viewSize / horizontalCellsCount , 0f, x.toFloat() * viewSize / horizontalCellsCount, viewSize.toFloat(), paint)
        }
        for (y in 0..verticalCellsCount + 1)
            canvas.drawLine(0f, y.toFloat() * viewSize / verticalCellsCount, viewSize.toFloat(), y.toFloat() * viewSize / verticalCellsCount, paint)
    }

    inner class LongPressGestureListener : GestureDetector.SimpleOnGestureListener() {

        override fun onLongPress(e: MotionEvent) {
            super.onLongPress(e)
            val (x, y) = convertToRelative(e.x.toInt(), e.y.toInt())
            Timber.d("Long press %d %d", x, y)
            longPressListener?.cellPressed(x, y)
        }

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            val (x, y) = convertToRelative(e.x.toInt(), e.y.toInt())
            singleTapListener?.cellTapped(x, y)
            return super.onSingleTapUp(e)
        }
        override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
            Timber.d("SingleTap")
            return super.onSingleTapConfirmed(e)
        }
    }

    interface LongPressListener {
        fun cellPressed(x: Int, y: Int)
    }

    interface SingleTapListener {
        fun cellTapped(x: Int, y:Int)
    }
}