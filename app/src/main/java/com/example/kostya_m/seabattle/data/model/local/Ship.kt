package com.example.kostya_m.seabattle.data.model.local

import android.os.Parcel
import android.os.Parcelable
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation

/**
 * Created by Kostya_M on 30.10.2016.
 */
data class Ship(var x: Int?, var y: Int?, val length: Int, var orientation: ShipOrientation) : Parcelable {
    fun rotate() {
        orientation = if (orientation == ShipOrientation.VERTICAL) ShipOrientation.HORIZONTAL
        else ShipOrientation.VERTICAL
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Ship> = object : Parcelable.Creator<Ship> {
            override fun createFromParcel(source: Parcel): Ship = Ship(source)
            override fun newArray(size: Int): Array<Ship?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readInt(), source.readInt(), source.readInt(), ShipOrientation.values()[source.readInt()])

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(x ?: 0)
        dest?.writeInt(y ?: 0)
        dest?.writeInt(length)
        dest?.writeInt(orientation.ordinal)
    }
}