package com.example.kostya_m.seabattle.data.manager

import com.example.kostya_m.seabattle.data.model.remote.GameReadyUpdate
import com.example.kostya_m.seabattle.data.model.remote.PlayersCountChangedUpdate
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.exceptions.RoomFullException
import rx.Observable
import rx.Single
import rx.lang.kotlin.toSingletonObservable
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Kostya_M on 19.11.2016.
 */
@ConfigPersistent
class RoomManagerStub
@Inject constructor() : RoomManager() {

    private var it = 1
    private val rooms : MutableList<Room> = mutableListOf(emptyRoom(), filledRoom(), almostFilledRoom(), emptyRoom(), filledRoom())

    override fun getRoomList() : Single<List<Room>> {
        return Single.just(rooms)
    }

    override fun joinRoom(room: Room): Single<Room> {
        return if (room.isFull()) {
            meReady = null
            Single.error(RoomFullException())
        } else {
            meReady = false
            Single.just(room)
        }
    }

    override fun leaveRoom(room: Room): Single<Room> {
        meReady = null
        val foundRoom = rooms.filter { it.id == room.id }.first()
        val index = rooms.indexOf(foundRoom)
        rooms[index] = Room(room.id, room.owner, room.players.dropLast(1))
        return Single.just(room)
    }

    override fun createRoom(): Single<Room> {
        rooms.add(Room(it++, emptySession(), listOf(emptySession())))
        return Single.just(rooms.last())
    }

    override fun updateRoom(room: Room): Observable<GameReadyUpdate> = Observable.just(GameReadyUpdate(room.id, "1", 3))

    override fun getRoom(room: Room): Single<Room> {
        return Single.just(room)
    }

    override fun updatePlayers(room: Room): Observable<PlayersCountChangedUpdate> {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun emptyRoom() = Room(it++, emptySession(), emptyList())
    fun almostFilledRoom() = Room(it++, emptySession(), listOf(emptySession()))
    fun filledRoom() = Room(it++, emptySession(), listOf(emptySession(), emptySession()))
    fun emptySession() = Session("", "")
}