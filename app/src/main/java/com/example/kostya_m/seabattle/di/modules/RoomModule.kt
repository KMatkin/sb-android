package com.example.kostya_m.seabattle.di.modules

import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.ui.room.RoomListFragment
import dagger.Module
import dagger.Provides

/**
 * Created by Kostya_M on 18.11.2016.
 */
@Module
class RoomModule {
    @Provides
    @PerActivity
    fun provideRoomListFragment() : RoomListFragment {
        return RoomListFragment()
    }
}