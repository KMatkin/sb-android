package com.example.kostya_m.seabattle.ui.room

import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.PersistableBundle
import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.Toolbar
import android.view.View
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.remote.GameReadyUpdate
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.di.components.RoomComponent
import com.example.kostya_m.seabattle.di.modules.RoomModule
import com.example.kostya_m.seabattle.ui.base.BaseActivity
import com.example.kostya_m.seabattle.util.exceptions.RoomFullException
import com.example.kostya_m.seabattle.util.extension.*
import com.jakewharton.rxbinding.view.clicks
import kotlinx.android.synthetic.main.activity_room.*
import kotlinx.android.synthetic.main.fragment_room_list.*
import kotlinx.android.synthetic.main.progress_view.*
import org.jetbrains.anko.toast
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RoomActivity : BaseActivity(), RoomContract.View {
    @Inject
    lateinit var roomListFragment : RoomListFragment

    @Inject
    lateinit var presenter: RoomPresenter

    var roomComponent: RoomComponent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        saveGameToken("")
        setContentView(R.layout.activity_room)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        activityComponent.inject(this)
        roomComponent = BaseActivity.componentsMap[activityId]?.roomComponent(RoomModule())
        roomComponent?.inject(this)
        presenter.attachView(this)
        showRoomList()
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun onBackPressed() {
        when (supportFragmentManager.findFragmentById(R.id.fragmentContainer)) {
            is RoomInfoFragment -> showRoomList()
            else -> super.onBackPressed()
        }
    }

    override fun showRoomList() {
        setFabForList()
        toolbar?.navigationIcon = null
        supportActionBar?.title = getString(R.string.title_activity_room)
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, roomListFragment)
                .commit()
        roomFab.clicks()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    presenter.createRoom()
                }
    }

    override fun showRoomInfo(room: Room) {
        setFabReady()
        toolbar?.navigationIcon = this.getCompatDrawable(R.drawable.keyboard_backspace)
        supportActionBar?.title = String.format(getString(R.string.room_name_format), room.id)
        val fragment = RoomInfoFragment(room)
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
        roomFab.clicks()
                .subscribe {
                    fragment.fabClicked()
                }
    }

    override fun setReady(value: Boolean) {
        if (value) setFabUnready() else setFabReady()
    }

    override fun showRooms(rooms: List<Room>?) {
        roomListFragment.setRooms(rooms)
    }

    override fun showError(th: Throwable) {
        super.showError(th)
        when (supportFragmentManager.findFragmentById(R.id.fragmentContainer)) {
            is RoomListFragment -> roomListSwipeRefreshLayout?.isRefreshing = false
        }
    }

    override fun startGame(gameReadyUpdate: GameReadyUpdate) {
        saveGameToken(gameReadyUpdate.gameToken!!)
        navigator.showArrange(this, gameReadyUpdate.gameToken)
        showRoomList()
    }

    override fun hideProgress() {
        progressView.visibility = View.GONE
        roomFab.visibility = View.VISIBLE
    }

    override fun showProgress() {
        progressView.visibility = View.VISIBLE
        roomFab.visibility = View.INVISIBLE
    }

    override fun enemyConnected() {
        toast(getString(R.string.message_enemy_connected))
    }

    override fun enemyLeft() {
        toast(getString(R.string.message_enemy_left))
    }

    private fun saveGameToken(token: String) {
        sharedPreferences.putString(PREF_KEYS.PREF_GAME, token)
    }

    private fun setFabForList() = setFab(R.color.fabBlue, R.drawable.plus_white)

    private fun setFabReady() = setFab(R.color.fabGreen, R.drawable.check_white)

    private fun setFabUnready() = setFab(R.color.fabRed, R.drawable.dismiss_white)

    private fun setFab(@ColorInt color: Int, @DrawableRes drawable: Int) {
        roomFab.setImageDrawable(this.getCompatDrawable(drawable))
        roomFab.backgroundTintList = ColorStateList.valueOf(this.getCompatColor(color))
    }
}
