package com.example.kostya_m.seabattle.data.repo

import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.ShipCellsIterator
import javax.inject.Inject

/**
 * Created by Kostya_M on 30.10.2016.
 */
@ConfigPersistent
class ShipRepo @Inject constructor() {
    companion object {
        val ONE = 4
        val TWO = 3
        val THREE = 2
        val FOUR = 1
    }

    val ships: MutableList<Ship>

    init {
        ships = mutableListOf()
        for (i in 1..ONE)
            ships.add(Ship(null, null, 1, ShipOrientation.VERTICAL))
        for (i in 1..TWO)
            ships.add(Ship(null, null, 2, ShipOrientation.VERTICAL))
        for (i in 1..THREE)
            ships.add(Ship(null, null, 3, ShipOrientation.VERTICAL))
        for (i in 1..FOUR)
            ships.add(Ship(null, null, 4, ShipOrientation.VERTICAL))
    }

    fun getShipOn(x: Int, y: Int): Ship? {
        var ship = ships.find { it.x == x && it.y == y }
        // Check if ship cells belong (x,y)
        if (ship == null) {
            for (currentShip in ships.filter { it.x != null && it.y != null }) {
                for (point in ShipCellsIterator(currentShip.x!!, currentShip.y!!, currentShip.length, currentShip.orientation)) {
                    if (point.first == x && point.second == y) {
                        ship = currentShip
                        break
                    }
                }
            }
        }
        return ship
    }

    fun reset() {
        ships
                .filter {
                    it.x != null && it.y != null
                }
                .map {
                    it.x = null
                    it.y = null
                    it.orientation= ShipOrientation.VERTICAL
                }
    }

}