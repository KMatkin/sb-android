package com.example.kostya_m.seabattle

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.service.UserService
import com.example.kostya_m.seabattle.di.components.ApplicationComponent
import com.example.kostya_m.seabattle.di.components.DaggerApplicationComponent
import com.example.kostya_m.seabattle.di.modules.ApplicationModule
import com.example.kostya_m.seabattle.di.modules.DataModule
import com.example.kostya_m.seabattle.util.extension.showErrorToast
import org.jetbrains.anko.toast
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by Kostya_M on 23.10.2016.
 */
class SeaBattleApplication : Application() {
    lateinit var applicationComponent: ApplicationComponent
    val session : Session = Session("", "")

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        val appModule = ApplicationModule(this)
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(appModule)
                .dataModule(DataModule())
                .build()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        UserService(applicationComponent.seaBattleApi())
                .createUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .retryWhen { attempts -> attempts.flatMap{ Observable.timer(3, TimeUnit.SECONDS) }}
                .subscribe({
                    session.token = it.token
                    session.securityToken = it.securityToken
                }, {
                    this.showErrorToast(it)
                })
    }
}