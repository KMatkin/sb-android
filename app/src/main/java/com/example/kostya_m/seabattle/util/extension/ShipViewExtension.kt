package com.example.kostya_m.seabattle.util.extension

import android.content.ClipData
import android.widget.ImageView
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation

/**
 * Created by Kostya_M on 30.10.2016.
 */

object ShipClipData {
    val ARG_SHIP_LENGHT = 0
    val ARG_VIEW_DIMENSIONS = 1
    val ARG_SHIP_ORIENTATION = 2
}

fun ImageView.getClipData() : ClipData {
    val ship = this.tag as Ship
    val data = ClipData.newPlainText("ShipLength", ship.length.toString())
    data.addItem(ClipData.Item("%d %d".format(this.width, this.height)))
    data.addItem(ClipData.Item(ship.orientation.name))
    return data
}