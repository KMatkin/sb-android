package com.example.kostya_m.seabattle.util.extension

/**
 * Created by Kostya_M on 02.11.2016.
 */

fun Float.isZeroFraction(): Boolean {
    return this.toInt().toFloat() == this
}
