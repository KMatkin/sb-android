package com.example.kostya_m.seabattle.util.extension

import java.util.*

/**
 * Created by Kostya_M on 02.12.2016.
 */
fun Array<out Any>.randomItem() : Any? {
    val r = Random()
    return this[r.nextInt(this.size)]
}

fun <T> List<T>.randomItem() : T? {
    val r = Random()
    return this[r.nextInt(this.size)]
}
