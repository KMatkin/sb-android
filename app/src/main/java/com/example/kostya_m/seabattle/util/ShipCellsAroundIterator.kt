package com.example.kostya_m.seabattle.util

import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import timber.log.Timber

/**
 * Created by Kostya_M on 03.11.2016.
 */
class ShipCellsAroundIterator(val x: Int, val y: Int, val shipLength: Int, val orientation: ShipOrientation):
        AbstractIterator<Pair<Int, Int>>() {
    companion object {
        val LEFT_AND_RIGHT_CELLS_COUNT = 6
    }
    private var i = 0
    private var currentX = x-1
    private var currentY = y-1
    private var currentDirection = Direction.RIGHT
    private val rotateDirectionCells: Array<Pair<Int,Int>>
    private val shipWidth = if (orientation == ShipOrientation.HORIZONTAL) shipLength else 1
    private val shipHeight = if (orientation == ShipOrientation.VERTICAL) shipLength else 1

    init {
        rotateDirectionCells = arrayOf(
                Pair(x-1, y-1),
                Pair(x+shipWidth, y-1),
                Pair(x+shipWidth, y+shipHeight),
                Pair(x-1, y+shipHeight))
    }

    override fun computeNext() {
        if (i == shipLength * 2 + LEFT_AND_RIGHT_CELLS_COUNT) {
            done()
        } else {
            setNext(Pair(currentX, currentY))

            when (currentDirection) {
                ShipCellsAroundIterator.Direction.RIGHT -> currentX++
                ShipCellsAroundIterator.Direction.DOWN -> currentY++
                ShipCellsAroundIterator.Direction.LEFT -> currentX--
                ShipCellsAroundIterator.Direction.UP -> currentY--
            }
            if (rotateDirectionCells.contains(Pair(currentX, currentY))) {
                currentDirection++
            }
            i++
        }
    }

    enum class Direction {
        RIGHT, DOWN, LEFT, UP;
        operator fun inc() : Direction {
            val nextDir: Direction
            when (this) {
                ShipCellsAroundIterator.Direction.RIGHT -> nextDir = Direction.DOWN
                ShipCellsAroundIterator.Direction.DOWN -> nextDir = Direction.LEFT
                ShipCellsAroundIterator.Direction.LEFT -> nextDir = Direction.UP
                ShipCellsAroundIterator.Direction.UP -> nextDir = Direction.RIGHT
            }
            return nextDir
        }
    }
}

fun aroundShipCells(x : Int, y : Int, length: Int, orientation: ShipOrientation, func: (Int, Int) -> Unit) {
    for (point in ShipCellsAroundIterator(x, y, length, orientation)) {
        func(point.first, point.second)
    }
}