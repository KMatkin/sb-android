package com.example.kostya_m.seabattle.ui.game

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.data.repo.GameFieldRepo
import com.example.kostya_m.seabattle.ui.widgets.GameFieldView
import kotlinx.android.synthetic.main.game_field_fragment.*
import org.jetbrains.anko.support.v4.find
import timber.log.Timber
import java.util.*


/**
 * Created by Kostya_M on 15.11.2016.
 */
class GameFieldFragment : Fragment() {
    companion object {
        val ARG_ENEMY = "ARG_ENEMY"
        val ARG_CELLS = "ARG_CELLS"
        fun newInstance(isEnemy: Boolean): Fragment {
            val fragment = GameFieldFragment()
            val args = Bundle()
            args.putBoolean(ARG_ENEMY, isEnemy)
            fragment.arguments = args
            return fragment
        }

        fun newInstance(isEnemy: Boolean, cells: Array<CellType>): Fragment {
            val fragment = GameFieldFragment()
            val args = Bundle()
            args.putBoolean(ARG_ENEMY, isEnemy)
            args.putIntArray(ARG_CELLS, cells.map { it.value }.toIntArray())
            fragment.arguments = args
            return fragment
        }
    }

    var isEnemy = false
    var cells : List<CellType>? = null
    var field : GameFieldView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isEnemy = arguments?.getBoolean(ARG_ENEMY) ?: false
        if (!isEnemy) {
            val cells = arguments?.getIntArray(ARG_CELLS)?.map { CellType.values()[it] }
            this.cells = cells
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater?.inflate(R.layout.game_field_fragment, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        field = view?.findViewById(R.id.gameField) as GameFieldView
        if (isEnemy) {
            field?.singleTapListener = activity as GameFieldView.SingleTapListener
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isEnemy) {
            if (cells != null) {
                field?.initCells = cells!!.mapIndexed {
                    i, cellType -> Pair(Pair(i.mod(GameFieldRepo.DEFAULT_SIZE), i.div(GameFieldRepo.DEFAULT_SIZE)), cellType)
                }
                cells = null
            }
        }
    }

    fun showCell(cellType: CellType, x: Int, y: Int) {
        field?.fillCell(x, y, cellType)
    }
}