package com.example.kostya_m.seabattle.ui.arrange

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.DragEvent
import android.view.View
import android.view.ViewTreeObserver
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import com.example.kostya_m.seabattle.di.modules.GameModule
import com.example.kostya_m.seabattle.ui.base.BaseActivity
import com.example.kostya_m.seabattle.ui.widgets.GameFieldView
import com.jakewharton.rxbinding.view.clicks
import kotlinx.android.synthetic.main.activity_arrange.*
import kotlinx.android.synthetic.main.progress_view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Kostya_M on 29.10.2016.
 */
class ArrangeActivity : BaseActivity(), ArrangeContract.View, GameFieldView.LongPressListener,
        ShipAdapter.DragShipListener {
    companion object {
        @JvmStatic
        val ARG_GAME_TOKEN : String = "GAME_TOKEN"
    }

    @Inject
    lateinit var presenter: ArrangePresenter
    @Inject
    lateinit var shipAdapter: ShipAdapter

    private var recyclerWidth: Int = 0
    private var itemWidth: Float = 0f
    private var shipPadding: Float = 0f
    private var firstItemWidth: Float = 0f
    private var allPixels: Int = 0
    private var selectedShipIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arrange)
        title = getString(R.string.title_activity_arrange)
        activityComponent.inject(this)
        BaseActivity.componentsMap[activityId]?.gameComponent(GameModule())?.inject(this)
        presenter.gameToken = intent.extras?.getString(ARG_GAME_TOKEN)
        Timber.i("Game token is %s", presenter.gameToken)
        presenter.attachView(this)
        shipAdapter.ships = presenter.getShips().toMutableList()

        initShipRecycler()
        arrangeGameField.longPressListener = this
        arrangeRotateView.clicks()
                .subscribe{
                    val ship = shipAdapter.getSelectedShip()
                    if (ship != null)
                    presenter.rotateShip(ship)
                }
        arrangeResetView.clicks()
                .subscribe {
                    presenter.resetField()
                }
        arrangeStartView.clicks()
                .subscribe {
                    presenter.startFight()
                }
        arrangeGenerateFieldView.clicks()
                .subscribe {
                    presenter.generateField()
                }
        shipAdapter.dragListener = this
    }

    override fun onResume() {
        super.onResume()
        presenter.updateGameState()
    }

    override fun onBackPressed() {
        alert("Вы действительно хотите выйти?", "Выход") {
            positiveButton("Да") { super.onBackPressed() }
            negativeButton("Нет")
        }.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unsubscribeToGame()
        presenter.detachView()
    }

    override fun updateCell(x: Int, y: Int, value: Boolean) {
        arrangeGameField.fillCell(x, y, if (value) CellType.FILL else CellType.EMPTY)
    }

    override fun removeShipFromAvailable(ship: Ship) {
        // Check if this is the last ship
        if (selectedShipIndex == shipAdapter.itemCount - 2 && shipAdapter.itemCount - 2 != 1) {
            allPixels -= itemWidth.toInt()
        }
        shipAdapter.removeItem(ship)
        setShipValue()
    }

    override fun showReady(value: Boolean) {
        arrangeGenerateFieldView.visibility = if (value) View.GONE else View.VISIBLE
        arrangeStartView.visibility = if (value) View.VISIBLE else View.GONE
    }

    override fun cellPressed(x: Int, y: Int) {
        Timber.d("Cell Pressed %d %d", x, y)
        presenter.removeShipOn(x, y)
        // Удалить заполненные ячейки с поля
        // Создать ImageView
        // Начать драг
        // Если драг сфейлился, то добавить корабль к списку доступных
    }

    override fun updateAvailableShips() {
        shipAdapter.notifyDataSetChanged()
    }

    override fun addShipsToAvailable(ships: Array<Ship>) {
        val selectFirst = shipAdapter.itemCount == 2
        shipAdapter.addShips(ships)
        if (selectFirst) {
            calculatePositionAndScrollShip(arrangeShipRecycler)
        }
    }

    override fun showFight(ships: Array<Ship>, myTurn: Boolean) {
        navigator.showGame(this, presenter.gameToken!!, ships, myTurn)
        finish()
    }

    override fun closeFight() {
        toast("Something gone wrong...")
        finish()
    }

    override fun dragStarted() {
//        canScrollShips = false
    }

    override fun dragEnded() {
//        canScrollShips = true
    }

    fun initShipRecycler() {
        val recyclerView = arrangeShipRecycler
        recyclerView.postDelayed({
            setShipValue()
        }, 300)
        val vtoShip = recyclerView.viewTreeObserver
        vtoShip.addOnPreDrawListener(ShipsOnPreDrawListener())
    }

    //http://stackoverflow.com/questions/34586637/get-center-visible-item-of-recycleview-when-scrolling
    inner class ShipsOnPreDrawListener: ViewTreeObserver.OnPreDrawListener {
        override fun onPreDraw(): Boolean {
            val recyclerView = arrangeShipRecycler
            recyclerView.viewTreeObserver.removeOnPreDrawListener(this)
            recyclerWidth = recyclerView.measuredWidth
            itemWidth = resources.getDimension(R.dimen.ship_item_width)
            shipPadding = (recyclerWidth - itemWidth) / 2
            firstItemWidth = shipPadding
            allPixels = 0

            recyclerView.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    synchronized(this) {
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            calculatePositionAndScrollShip(recyclerView!!)
                        }
                    }
                }
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    allPixels += dx
                }
            })
            shipAdapter.paddingWidth = firstItemWidth.toInt()
            recyclerView.adapter = shipAdapter
            shipAdapter.setSelectedItem(shipAdapter.itemCount - 1)
            arrangeGameField.setOnDragListener { view, dragEvent ->
                var result = true
                when(dragEvent.action) {
                    DragEvent.ACTION_DROP -> {
                        val oldView = dragEvent.localState as View
                        val ship = oldView.tag as Ship
                        var (x, y) = Pair(0f,0f)
                        when (ship.orientation) {
                            ShipOrientation.HORIZONTAL -> {
                                x = dragEvent.x - oldView.width/2 + oldView.width.toFloat()/ship.length*0.6f
                                y = dragEvent.y
                            }
                            ShipOrientation.VERTICAL -> {
                                x = dragEvent.x
                                y = dragEvent.y - oldView.height/2 + oldView.height/(ship.length*2)
                            }
                        }

                        val (leftCell, topCell) = arrangeGameField.convertToRelative(x.toInt(), y.toInt())
                        // Correct place
                        if (presenter.placeShip(leftCell, topCell, ship)) {
                        } else {
                            toast("Нельзя разместить корабль здесь")
                            oldView.visibility = View.VISIBLE
                            result = false
                        }
                        dragEnded()
                    }
                    DragEvent.ACTION_DRAG_ENTERED -> Timber.d("Action drag entered")
                    DragEvent.ACTION_DRAG_EXITED -> Timber.d("Action drag exited")
                    DragEvent.ACTION_DRAG_ENDED -> {
                        Timber.d("Action drag ended")
                        (dragEvent.localState as View).visibility = View.VISIBLE
                        dragEnded()
                    }
                }
                result
            }
            return true
        }
    }

    /* this if most important, if expectedPositionDate < 0 recyclerView will return to nearest item*/

    private fun calculatePositionAndScrollShip(recyclerView: RecyclerView) {
        var expectedPositionShip = Math.round((allPixels + shipPadding - firstItemWidth) / itemWidth)

        if (expectedPositionShip == -1) {
            expectedPositionShip = 0
        } else if (expectedPositionShip >= recyclerView.adapter.itemCount - 2 && expectedPositionShip >= 1) {
            expectedPositionShip--
        }
        scrollListToPositionShip(recyclerView, expectedPositionShip)
    }

    /* this if most important, if expectedPositionShip < 0 recyclerView will return to nearest item*/
    private fun scrollListToPositionShip(recyclerView: RecyclerView, expectedPositionShip: Int) {
        val targetScrollPosShip = expectedPositionShip * itemWidth + firstItemWidth - shipPadding
        val missingPxShip = targetScrollPosShip - allPixels
        if (missingPxShip != 0f) {
            if (!(missingPxShip < 0 && allPixels < 10) && !(missingPxShip > 0 && missingPxShip < 10))
                recyclerView.smoothScrollBy(missingPxShip.toInt(), 0)
        }
        setShipValue()
    }

    private fun setShipValue() {
        val expectedPosition = Math.round((allPixels + shipPadding - firstItemWidth) / itemWidth)
        selectedShipIndex = expectedPosition + 1
        shipAdapter.setSelectedItem(selectedShipIndex)
    }

    override fun showProgress() {
        progressView.visibility = View.VISIBLE
    }
}