package com.example.kostya_m.seabattle.ui.room

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.util.RecyclerItemClickListener
import kotlinx.android.synthetic.main.fragment_room_list.*
import javax.inject.Inject

@PerActivity
class RoomListFragment
@Inject constructor() : Fragment() {

    @Inject lateinit var presenter : RoomPresenter
    @Inject lateinit var adapter : RoomAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_room_list, container, false)
        val roomActivity = activity as RoomActivity
        roomActivity.roomComponent?.inject(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        roomListSwipeRefreshLayout.setOnRefreshListener {
            presenter.requestRoomList()
        }
        roomListRecyclerView.layoutManager = LinearLayoutManager(context)
        roomListRecyclerView.adapter = adapter
        roomListRecyclerView.addOnItemTouchListener(RecyclerItemClickListener(activity, roomListRecyclerView,
            object : RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    presenter.joinRoom(adapter.items!![position])
                }

                override fun onItemLongClick(view: View, position: Int) {
                    // ignore
                }
        }))
        presenter.requestRoomList()
    }

    fun setRooms(rooms: List<Room>?) {
        adapter.items = rooms?.toMutableList()
        roomListSwipeRefreshLayout.isRefreshing = false
    }
}
