package com.example.kostya_m.seabattle.ui.arrange

import com.example.kostya_m.seabattle.data.manager.ArrangeManager
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import com.example.kostya_m.seabattle.data.model.remote.GameStates
import com.example.kostya_m.seabattle.data.repo.ArrangeFieldRepo
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.ui.base.BaseMvpPresenter
import com.example.kostya_m.seabattle.util.ShipCellsIterator
import com.example.kostya_m.seabattle.util.exceptions.GameNotReady
import com.example.kostya_m.seabattle.util.shipCells
import rx.Observable
import rx.lang.kotlin.deferredObservable
import rx.lang.kotlin.observable
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Kostya_M on 29.10.2016.
 */
@ConfigPersistent
class ArrangePresenter
@Inject
constructor(val arrangeManager: ArrangeManager): ArrangeContract.Presenter() {
    var gameToken : String? = null
    set(value) {
        field = value
        arrangeManager.gameToken = value
    }

    private val subscriptions = CompositeSubscription()

    override fun getShips(): List<Ship> = arrangeManager.getShips()

    private fun isFieldReady(): Boolean {
        return arrangeManager.getShips().all { it.x != null && it.y != null }
    }

    override fun placeShip(x: Int, y: Int, ship: Ship): Boolean {
        val result: Boolean
        if (arrangeManager.checkPlace(x, y, ship.length, ship.orientation)) {
            // Update model
            arrangeManager.placeShip(x, y, ship)
            // Update view
            placeShipInView(x, y, ship)
            result = true
        } else {
            result = false
        }
        view.showReady(isFieldReady())
        return result
    }

    private fun placeShipInView(x: Int, y: Int, ship: Ship) {
        // Fill cells
        shipCells(x, y, ship.length, ship.orientation, { x, y ->
            view.updateCell(x, y, true)
        })
        // Update available ships
        view.removeShipFromAvailable(ship)
    }

    override fun rotateShip(ship: Ship) {
        ship.rotate()
        view.updateAvailableShips()
    }

    override fun removeShipOn(x: Int, y: Int) {
        val ship = arrangeManager.getShipOn(x, y)
        if (ship != null) {
            shipCells(ship.x!!, ship.y!!, ship.length, ship.orientation, { x, y ->
                // Update model
                arrangeManager.setCell(x, y, false)
                // Erase cells
                view.updateCell(x, y, false)
            })
            ship.x = null
            ship.y = null
            view.addShipsToAvailable(arrayOf(ship))
            view.updateAvailableShips()
        }
        view.showReady(isFieldReady())
    }

    override fun resetField() {
        Timber.d("Reset field")
        view.addShipsToAvailable(arrangeManager.getShips().filter { it.x != null && it.y != null }.toTypedArray())
        view.updateAvailableShips()
        (0..ArrangeFieldRepo.DEFAULT_SIZE)
                .flatMap { y -> (0..ArrangeFieldRepo.DEFAULT_SIZE).map { x-> Pair(x, y) } }
                .forEach { view.updateCell(it.first, it.second, false) }
        arrangeManager.reset()
        view.showReady(isFieldReady())
    }

    override fun startFight() {
        view.showProgress()
        arrangeManager.sendShips()
                .subscribe({
                }, { e->
                    view.closeFight()
                })
    }

    override fun generateField() {
        arrangeManager.generateField()
        arrangeManager.getShips().forEach {
            ship -> placeShipInView(ship.x!!, ship.y!!, ship)
        }
        view.showReady(isFieldReady())
    }

    fun updateGameState() {
        subscriptions.add(arrangeManager.gameUpdate()
                .timeout(30, TimeUnit.SECONDS)
                .retryWhen { attempts ->
                    attempts.filter {
                        it is SocketTimeoutException
                    }
                }
                .subscribe({
                    view.showFight(arrangeManager.getShips().toTypedArray(), it.gameInfo.isMyTurn)
                }, { e ->
                    Timber.e("Something gone wrong...")
                    view.closeFight()
                }))
    }

    override fun unsubscribeToGame() {
        subscriptions.clear()
    }
}