package com.example.kostya_m.seabattle.di.components

import android.app.Activity
import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.di.modules.ActivityModule
import com.example.kostya_m.seabattle.ui.arrange.ArrangeActivity
import com.example.kostya_m.seabattle.ui.end.FinishScreenActivity
import com.example.kostya_m.seabattle.ui.game.GameActivity
import com.example.kostya_m.seabattle.ui.login.LoginActivity
import com.example.kostya_m.seabattle.ui.room.RoomActivity
import dagger.Subcomponent


/**
 * Created by Kostya_M on 23.10.2016.
 */

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(loginActivity: LoginActivity)
    fun inject(arrangeActivity: ArrangeActivity)
    fun inject(gameActivity: GameActivity)
    fun inject(roomActivity: RoomActivity)
    fun inject(finishScreenActivity: FinishScreenActivity)
}