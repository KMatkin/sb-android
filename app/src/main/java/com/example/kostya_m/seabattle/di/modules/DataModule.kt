package com.example.kostya_m.seabattle.di.modules

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.support.annotation.Nullable
import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.service.SeaBattleApi
import com.example.kostya_m.seabattle.data.service.UserService
import com.example.kostya_m.seabattle.di.ApplicationContext
import dagger.Module
import dagger.Provides
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Kostya_M on 26.10.2016.
 */
@Module(includes = arrayOf(ApiModule::class))
class DataModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(app: SeaBattleApplication) : SharedPreferences {
        return app.getSharedPreferences("seaBattle", MODE_PRIVATE)
    }

    @Provides
    fun provideCurrentSession(application: SeaBattleApplication) : Session {
        return application.session
    }
}