package com.example.kostya_m.seabattle.data.manager

import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import com.example.kostya_m.seabattle.data.model.remote.GameInfoUpdate
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.repo.ArrangeFieldRepo
import com.example.kostya_m.seabattle.data.repo.ShipRepo
import com.example.kostya_m.seabattle.data.service.GameService
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.aroundShipCells
import com.example.kostya_m.seabattle.util.extension.randomItem
import com.example.kostya_m.seabattle.util.shipCells
import rx.Observable
import rx.Single
import java.util.*
import javax.inject.Inject

/**
 * Created by Kostya_M on 29.10.2016.
 */
@ConfigPersistent
class ArrangeManager
@Inject constructor(private val fieldRepo: ArrangeFieldRepo, private val shipRepo: ShipRepo,
                    private val gameService: GameService, val session: dagger.Lazy<Session>) {

    var gameToken : String? = null

    fun generateField() {
        reset()
        var freeCells = fieldRepo.field.mapIndexed { i, b -> Pair(i.mod(ArrangeFieldRepo.DEFAULT_SIZE), i.div(ArrangeFieldRepo.DEFAULT_SIZE)) }

        try {
            for(ship in shipRepo.ships.sortedBy(Ship::length)) {
                // Try to place ship in random cell
                for (tr in 0..100) {
                    if (Random().nextBoolean())
                        ship.rotate()
                    val randomCell = freeCells.randomItem()
                    var placed = tryPlaceShip(ship, randomCell!!.first, randomCell.second)
                    if (!placed) {
                        ship.rotate()
                        placed = tryPlaceShip(ship, randomCell.first, randomCell.second)
                    }
                    if (placed) {
                        // Remove from free cells
                        shipCells(ship.x!!, ship.y!!, ship.length, ship.orientation, {
                            x, y ->
                            freeCells = freeCells.filter { it != randomCell }
                        })
                        break
                    }
                    // Too many tries.. restart
                    if (tr == 99)
                        throw Exception("Nice try!")
                }
            }
        } catch(e: Exception) {
            generateField()
        }
    }

    fun tryPlaceShip(ship: Ship, x: Int, y: Int) : Boolean {
        var res = false
        if (checkPlace(x, y, ship.length, ship.orientation)) {
            placeShip(x, y, ship)
            res = true
        }
        return res
    }

    fun checkPlace(x: Int, y: Int, shipLength: Int, shipOrientation: ShipOrientation): Boolean {
        var result = true
        val size = ArrangeFieldRepo.DEFAULT_SIZE
        // Check ship cell
        shipCells(x, y, shipLength, shipOrientation,  { x, y ->
            if (!(x in 0..size-1 && y in 0..size-1) || fieldRepo.field[y * size + x]) {
                result = false
            }
        })
        if (result) {
            // Check cells around ship
            aroundShipCells(x, y, shipLength, shipOrientation, { x, y ->
                if (x in 0..size-1 && y in 0..size-1 && fieldRepo.field[y * size + x]) {
                    result = false
                }
            })
        }
        return result
    }

    fun placeShip(x: Int, y: Int, ship: Ship) {
        ship.x = x
        ship.y = y
        // Set cells as filled
        shipCells(x, y, ship.length, ship.orientation, { x, y ->
            setCell(x, y, true)
        })
//        // Remove ship from available ships
//        shipRepo.ships.remove(ship)
    }

    fun setCell(x: Int, y: Int, value: Boolean) {
        fieldRepo.field[y*ArrangeFieldRepo.DEFAULT_SIZE + x] = value
    }

    fun getShips() : List<Ship> = shipRepo.ships

    fun getShipOn(x: Int, y: Int) = shipRepo.getShipOn(x, y)

    fun reset() {
        shipRepo.reset()
        fieldRepo.reset()
    }

    fun sendShips() : Single<Any> {
        return gameService.setShips(gameToken!!, shipRepo.ships, session.get())
    }

    fun gameUpdate() : Observable<GameInfoUpdate> {
        return gameService.getGameUpdates(gameToken!!, session.get())
    }
}