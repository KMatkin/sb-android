package com.example.kostya_m.seabattle.ui.login

import com.example.kostya_m.seabattle.ui.base.BaseMvpPresenter
import com.example.kostya_m.seabattle.ui.base.MvpView

/**
 * Created by Kostya_M on 26.10.2016.
 */
object LoginContract {

    interface View: MvpView

    abstract class Presenter: BaseMvpPresenter<View>()
}