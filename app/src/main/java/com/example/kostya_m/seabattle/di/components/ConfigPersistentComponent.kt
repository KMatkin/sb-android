package com.example.kostya_m.seabattle.di.components

import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.di.modules.ActivityModule
import com.example.kostya_m.seabattle.di.modules.GameModule
import com.example.kostya_m.seabattle.di.modules.RoomModule
import dagger.Component

/**
 * Created by Kostya_M on 26.10.2016.
 */
@ConfigPersistent
@Component(dependencies = arrayOf(ApplicationComponent::class))
interface ConfigPersistentComponent {
    fun activityComponent(activityModule: ActivityModule) : ActivityComponent
    fun gameComponent(gameModule: GameModule) : GameComponent
    fun roomComponent(roomModule: RoomModule) : RoomComponent
}