package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 20.11.2016.
 */
object GameStates {
    val Preparing = 1
    val Running = 2
    val Finished = 3
}