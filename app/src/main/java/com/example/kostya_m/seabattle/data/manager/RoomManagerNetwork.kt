package com.example.kostya_m.seabattle.data.manager

import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.data.model.remote.GameReadyUpdate
import com.example.kostya_m.seabattle.data.model.remote.PlayersCountChangedUpdate
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.data.service.RoomService
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.di.PerActivity
import rx.Observable
import rx.Single
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Kostya_M on 19.11.2016.
 */
@ConfigPersistent
class RoomManagerNetwork
@Inject constructor(private val roomService: RoomService, val session: dagger.Lazy<Session>): RoomManager() {

    override fun createRoom(): Single<Room> {
        return roomService.createRoom(session.get())
    }

    override fun joinRoom(room: Room): Single<Room> {
        meReady = null
        return roomService.joinRoom(room, session.get())
    }

    override fun leaveRoom(room: Room): Single<Room> {
        meReady = null
        return roomService.leaveRoom(room, session.get())
    }

    override fun getRoomList(): Single<List<Room>> {
        return roomService.getRoomList()
    }

    override fun setReady(room: Room) : Single<Room> {
        return super.setReady(room)
                .flatMapObservable { room ->
                    if (getReady()!!)
                        roomService.ready(room, session.get()).toObservable()
                    else
                        roomService.unready(room, session.get()).toObservable()
                }
                .toSingle()
    }

    override fun updateRoom(room: Room): Observable<GameReadyUpdate> {
        return roomService.updateRoomInfo(room, session.get())
    }

    override fun updatePlayers(room: Room) : Observable<PlayersCountChangedUpdate> {
        return roomService.updatePlayerInfo(room)
    }

    override fun getRoom(room: Room): Single<Room> {
        Timber.d("Try to get current room state %s", room.toString())
        return roomService.getRoomList()
                .map { rooms -> rooms.filter{it.id == room.id}.firstOrNull() }
    }
}