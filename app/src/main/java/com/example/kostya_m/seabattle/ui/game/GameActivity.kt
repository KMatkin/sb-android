package com.example.kostya_m.seabattle.ui.game

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.view.View
import android.view.animation.AnimationUtils
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.manager.GameManager
import com.example.kostya_m.seabattle.data.model.local.CellType
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.repo.GameFieldRepo
import com.example.kostya_m.seabattle.di.modules.GameModule
import com.example.kostya_m.seabattle.ui.base.BaseActivity
import com.example.kostya_m.seabattle.ui.widgets.GameFieldView
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.collections.forEachByIndex
import org.jetbrains.anko.toast
import org.jetbrains.anko.vibrator
import javax.inject.Inject

/**
 * Created by Kostya_M on 15.11.2016.
 */
class GameActivity : BaseActivity(), GameContract.View, GameFieldView.SingleTapListener {

    companion object {
        @JvmStatic
        val ARG_GAME_TOKEN = "GAME_TOKEN"
        @JvmStatic
        val ARG_SHIPS = "SHIPS"
        @JvmStatic
        val ARG_MY_TURN = "MY_TURN"
    }

    @Inject
    lateinit var presenter: GamePresenter

    lateinit var fieldsAdapter: FieldsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        setSupportActionBar(gameToolbar)
        activityComponent.inject(this)
        BaseActivity.componentsMap[activityId]?.gameComponent(GameModule())?.inject(this)
        presenter.attachView(this)
        presenter.gameToken = intent.extras?.getString(ARG_GAME_TOKEN)
        showStepMessage(intent.extras?.getBoolean(ARG_MY_TURN) ?: false)
        presenter.gameManager.gameToken = presenter.gameToken
        val ships = intent.extras?.getParcelableArray(ARG_SHIPS)?.map { it as Ship }!!
        presenter.gameManager.setMyShips(ships)

        supportActionBar?.title = getString(R.string.title_activity_game)
        fieldsAdapter = FieldsAdapter(this, supportFragmentManager)
        fieldsAdapter.myCells = presenter.gameManager.myFieldRepo.field
        gameFieldsPager.adapter = fieldsAdapter
        gameTabLayout.setupWithViewPager(gameFieldsPager)
        gameFieldsPager.currentItem = 1
    }

    override fun onResume() {
        super.onResume()
        presenter.subscribeToGameUpdates()
    }

    override fun onBackPressed() {
        alert("Вы действительно хотите выйти?", "Выход") {
            positiveButton("Да") { super.onBackPressed() }
            negativeButton("Нет")
        }.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
        presenter.unsubscribeToGameUpdates()
    }

    override fun showMyField(cells: Array<CellType>) {

    }

    override fun showEnemyCell(cellType: CellType, x: Int, y: Int) {
        val gameFieldFragment = fieldsAdapter.getItem(FieldsAdapter.ENEMY_FIELD) as GameFieldFragment
        gameFieldFragment.showCell(cellType, x, y)
    }

    override fun showMyCell(cellType: CellType, x: Int, y: Int) {
        val gameFieldFragment = fieldsAdapter.getItem(FieldsAdapter.MY_FIELD) as GameFieldFragment
        gameFieldFragment.showCell(cellType, x, y)
    }

    override fun showStepMessage(myTurn: Boolean) {
        if (myTurn) {
            gameMyTurn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))
            if (vibrator.hasVibrator()) {
                vibrator.vibrate(1000)
            }
        }
//        gameFieldsPager.setCurrentItem(if (!myTurn) FieldsAdapter.MY_FIELD else FieldsAdapter.ENEMY_FIELD, true)
        gameMyTurn.visibility = if (myTurn) View.VISIBLE else View.INVISIBLE
        gameEnemyTurn.visibility = if (myTurn) View.INVISIBLE else View.VISIBLE
    }

    override fun cellTapped(x: Int, y: Int) {
        presenter.makeStep(x, y)
    }

    override fun gameFinished(youWon: Boolean) {
        navigator.showResult(this, youWon)
        finish()
    }
}