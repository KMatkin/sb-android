package com.example.kostya_m.seabattle.ui.room

import android.content.SharedPreferences
import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.data.manager.RoomManagerNetwork
import com.example.kostya_m.seabattle.data.manager.RoomManagerStub
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.data.model.remote.Session
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.ui.base.ShowError
import com.example.kostya_m.seabattle.util.exceptions.RoomCreateException
import com.example.kostya_m.seabattle.util.exceptions.RoomFullException
import dagger.Lazy
import dagger.internal.Preconditions
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.lang.kotlin.deferredObservable
import rx.lang.kotlin.filterNotNull
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import timber.log.Timber
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Kostya_M on 18.11.2016.
 */
@ConfigPersistent
class RoomPresenter
@Inject constructor(private val roomManager: RoomManagerNetwork)
    : RoomContract.Presenter() {

    private val subscriptions = CompositeSubscription()

    override fun detachView() {
        super.detachView()
        subscriptions.unsubscribe()
    }

    override fun requestRoomList() {
        roomManager.getRoomList()
                .subscribe({
                    view.showRooms(it)
                }, {
                    showError(it)
                    view.showRooms(null)
                })
    }

    override fun requestSetReady(room: Room) {
        roomManager.setReady(room)
                .subscribe ({
                    view.setReady(roomManager.getReady()!!)
                }, {
                    showError(it)
                })
    }

    override fun getReady(): Boolean? {
        return roomManager.getReady()
    }

    override fun leaveRoom(room: Room) {
        roomManager.leaveRoom(room)
                .subscribe({
                    roomManager.currentRoom = null
                }, {Timber.d("Room doesn't exists")})

    }

    override fun joinRoom(room: Room) {
        roomManager.joinRoom(room)
                .doOnSubscribe {
                    view.showProgress()
                }
                .subscribe({
                    view.hideProgress()
                    view.showRoomInfo(it)
                }, {
                    view.hideProgress()
                    showError(it)
                })
    }

    override fun createRoom() {
        roomManager.createRoom()
                .doOnSubscribe {
                    view.showProgress()
                }
                .subscribe({
                    view.hideProgress()
                    view.showRoomInfo(it)
                }, {
                    view.hideProgress()
                    when(it) {
                        is IOException -> {
                            requestRoomList()
                        }
                        else -> showError(RoomCreateException())
                    }
                })
    }

    fun subscribeToRoomUpdates(room: Room) {
        subscriptions.clear()
        subscriptions.add(roomManager.updateRoom(room)
                .timeout(30, TimeUnit.SECONDS)
                .retryWhen { attempts ->
                    attempts.filter {
                        it is SocketTimeoutException || it is KotlinNullPointerException
                    }
                }
                .repeat()
                .takeUntil {
                    it?.gameToken != ""
                }
                .filter {
                    it?.gameToken != ""
                }
                .subscribe ({
                    view.startGame(it)
                }, {
                    showError(it)
//                    view.showRoomList()
                }))
        subscriptions.add(roomManager.updatePlayers(room)
                .timeout(30, TimeUnit.SECONDS)
                .retryWhen { attempts ->
                    attempts.filter {
                        it is SocketTimeoutException
                    }
                }
                .repeat()
                .takeUntil {
                    it.type != 3
                }
                .subscribe({
                    when(it.playersCount) {
                        2 -> view.enemyConnected()
                        1 -> view.enemyLeft()
                    }
                }))
    }

    fun unsubscribeToRoomUpdates() {
        subscriptions.clear()
    }

    private fun showError(th: Throwable) {
        (view as ShowError).showError(th)
    }
}