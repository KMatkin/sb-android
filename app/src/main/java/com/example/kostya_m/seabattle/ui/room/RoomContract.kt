package com.example.kostya_m.seabattle.ui.room

import com.example.kostya_m.seabattle.data.model.remote.GameReadyUpdate
import com.example.kostya_m.seabattle.data.model.remote.Room
import com.example.kostya_m.seabattle.ui.base.BaseMvpPresenter
import com.example.kostya_m.seabattle.ui.base.MvpView

/**
 * Created by Kostya_M on 18.11.2016.
 */
object RoomContract {
    interface View : MvpView {
        fun showRoomList()
        fun showRoomInfo(room: Room)
        fun setReady(value: Boolean)
        fun showRooms(rooms: List<Room>?)
        fun startGame(gameReadyUpdate: GameReadyUpdate)
        fun showProgress()
        fun hideProgress()
        fun enemyConnected()
        fun enemyLeft()
    }

    abstract class Presenter : BaseMvpPresenter<View>() {
        abstract fun requestRoomList()
        abstract fun requestSetReady(room: Room)
        abstract fun getReady() : Boolean?
        abstract fun leaveRoom(room: Room)
        abstract fun joinRoom(room: Room)
        abstract fun createRoom()
    }

}