package com.example.kostya_m.seabattle.data.model.remote

/**
 * Created by Kostya_M on 20.11.2016.
 */
data class MakeStepRequest(val gameToken: String, val token: String, val securityToken: String, val x: Int, val y: Int)
