package com.example.kostya_m.seabattle.data.repo

import com.example.kostya_m.seabattle.di.ConfigPersistent
import javax.inject.Inject

/**
 * Created by Kostya_M on 29.10.2016.
 */
@ConfigPersistent
class ArrangeFieldRepo @Inject constructor() {
    companion object {
        const val DEFAULT_SIZE = 10
    }
    var field: Array<Boolean> = Array(DEFAULT_SIZE * DEFAULT_SIZE, { false })

    fun reset() {
        field.fill(false, 0, ArrangeFieldRepo.DEFAULT_SIZE*ArrangeFieldRepo.DEFAULT_SIZE)
    }
}