package com.example.kostya_m.seabattle.ui.arrange

import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import com.example.kostya_m.seabattle.ui.base.BaseMvpPresenter
import com.example.kostya_m.seabattle.ui.base.MvpView

/**
 * Created by Kostya_M on 29.10.2016.
 */
object ArrangeContract {

    interface View: MvpView {
        fun updateCell(x: Int, y: Int, value: Boolean)
        fun removeShipFromAvailable(ship: Ship)
        fun addShipsToAvailable(ships: Array<Ship>)
        fun updateAvailableShips()
        fun showReady(value: Boolean)
        fun showFight(ships: Array<Ship>, myTurn: Boolean)
        fun closeFight()
        fun showProgress()
    }

    abstract class Presenter: BaseMvpPresenter<View>() {
        abstract fun getShips() : List<Ship>
        abstract fun placeShip(x: Int, y: Int, ship: Ship) : Boolean
        abstract fun rotateShip(ship: Ship)
        abstract fun removeShipOn(x: Int, y: Int)
        abstract fun resetField()
        abstract fun startFight()
        abstract fun unsubscribeToGame()
        abstract fun generateField()
    }
}