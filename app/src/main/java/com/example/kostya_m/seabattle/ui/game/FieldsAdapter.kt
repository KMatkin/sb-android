package com.example.kostya_m.seabattle.ui.game

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.local.CellType

/**
 * Created by Kostya_M on 15.11.2016.
 */
class FieldsAdapter(val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {
    companion object {
        @JvmStatic
        val FIELD_COUNT = 2
        @JvmStatic
        val MY_FIELD = 1
        @JvmStatic
        val ENEMY_FIELD = 0
    }

    var myCells : Array<CellType>? = null

    val fragments by lazy {
        arrayOf(GameFieldFragment.newInstance(true), GameFieldFragment.newInstance(false, myCells!!))
    }


    override fun getItem(position: Int): Fragment =
        fragments[position]

    override fun getCount(): Int =
        fragments.size

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            ENEMY_FIELD -> context.getString(R.string.enemy)
            MY_FIELD -> context.getString(R.string.me)
            else -> ""
        }
    }
}