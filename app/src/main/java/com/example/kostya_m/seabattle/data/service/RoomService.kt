package com.example.kostya_m.seabattle.data.service

import com.example.kostya_m.seabattle.data.model.remote.*
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.exceptions.RoomFullException
import rx.Observable
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Kostya_M on 19.11.2016.
 */
@ConfigPersistent
class RoomService
@Inject constructor(private val api: SeaBattleApi) {

    fun getRoomList() : Single<List<Room>> =
        api.getRoomList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())

    fun joinRoom(room: Room, session: Session) : Single<Room> {
        if (room.isFull()) {
            return Single.error(RoomFullException())
        } else {
            return api.joinRoom(room.id, session)
                    .map { room }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
        }
    }

    fun leaveRoom(room: Room, session: Session): Single<Room> =
        api.leaveRoom(room.id, session)
            .map { room }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())


    fun ready(room: Room, session: Session) : Single<Room> =
            api.ready(room.id, session)
                    .map { room }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())

    fun unready(room: Room, session: Session) : Single<Room> =
            api.unready(room.id, session)
                    .map { room }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())

    fun createRoom(session: Session) : Single<Room> =
            api.createRoom(session)
                    .map {
                        Room(it, session, listOf(session))
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())

    fun updateRoomInfo(room: Room, session: Session) : Observable<GameReadyUpdate> =
            api.roomInfo(room.id, session.token, session.securityToken)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())

    fun updatePlayerInfo(room: Room) : Observable<PlayersCountChangedUpdate> =
            api.playerUpdate(room.id)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
}