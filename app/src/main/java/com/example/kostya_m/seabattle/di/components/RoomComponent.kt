package com.example.kostya_m.seabattle.di.components

import com.example.kostya_m.seabattle.di.PerActivity
import com.example.kostya_m.seabattle.di.modules.RoomModule
import com.example.kostya_m.seabattle.ui.room.RoomActivity
import com.example.kostya_m.seabattle.ui.room.RoomInfoFragment
import com.example.kostya_m.seabattle.ui.room.RoomListFragment
import dagger.Subcomponent

/**
 * Created by Kostya_M on 18.11.2016.
 */
@PerActivity
@Subcomponent(modules = arrayOf(RoomModule::class))
interface RoomComponent {
    fun inject(roomActivity: RoomActivity)
    fun inject(roomActivity: RoomListFragment)
    fun inject(roomInfoFragment: RoomInfoFragment)
}