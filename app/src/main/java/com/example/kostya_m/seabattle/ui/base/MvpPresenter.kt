package com.example.kostya_m.seabattle.ui.base

/**
 * Created by Kostya_M on 26.10.2016.
 */
interface MvpPresenter<in V: MvpView> {
    fun attachView(view: V)
    fun detachView()
}