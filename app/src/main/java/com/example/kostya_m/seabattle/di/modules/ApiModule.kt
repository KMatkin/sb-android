package com.example.kostya_m.seabattle.di.modules

import com.example.kostya_m.seabattle.data.service.SeaBattleApi
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Kostya_M on 26.10.2016.
 */
@Module
class ApiModule {
    companion object {
        @JvmStatic
        val BASE_SERVICE_URL = "http://kilyakov.xyz:5050/api/"
    }

    @Provides
    @Singleton
    fun provideConverterFactory() : Converter.Factory {
        return MoshiConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideCallAdapterFactory() : CallAdapter.Factory {
        return RxJavaCallAdapterFactory.create()
    }

    @Provides
    @Singleton
    @Named("Logging")
    fun provideLoggingInterceptor() : Interceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }


    @Provides
    @Singleton
    fun provideOkHttpClient(@Named("Logging") loggingInterceptor: Interceptor) : OkHttpClient {
        val httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        return httpClient
    }

    @Provides
    @Singleton
    fun provideSeaBattleApi(okHttpClient: OkHttpClient,
                            converterFactory: Converter.Factory,
                            callAdapterFactory: CallAdapter.Factory) : SeaBattleApi {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_SERVICE_URL)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .build()
                .create(SeaBattleApi::class.java)
    }

}