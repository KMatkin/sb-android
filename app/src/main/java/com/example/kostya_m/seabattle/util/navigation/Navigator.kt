package com.example.kostya_m.seabattle.util.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.remote.GameReadyUpdate
import com.example.kostya_m.seabattle.di.ApplicationContext
import com.example.kostya_m.seabattle.ui.arrange.ArrangeActivity
import com.example.kostya_m.seabattle.ui.end.FinishScreenActivity
import com.example.kostya_m.seabattle.ui.game.GameActivity
import javax.inject.Inject
import javax.inject.Singleton
import org.jetbrains.anko.*

/**
 * Created by Kostya_M on 23.10.2016.
 */
@ApplicationContext
class Navigator @Inject constructor() {
    fun showGame(context: Context, gameToken: String, ships: Array<Ship>, myTurn: Boolean) {
        val intent = Intent(context, GameActivity::class.java)
        val b = Bundle()
        b.putString(GameActivity.ARG_GAME_TOKEN, gameToken)
        b.putParcelableArray(GameActivity.ARG_SHIPS, ships)
        b.putBoolean(GameActivity.ARG_MY_TURN, myTurn)
        intent.putExtras(b)
        context.startActivity(intent)
    }

    fun showArrange(context: Context, gameToken: String) {
        val intent = Intent(context, ArrangeActivity::class.java)
//        intent.clearTop()
        val b = Bundle()
        b.putString(ArrangeActivity.ARG_GAME_TOKEN, gameToken)
        intent.putExtras(b)
        context.startActivity(intent)
    }

    fun showResult(context: Context, youWon: Boolean) {
        val intent = Intent(context, FinishScreenActivity::class.java)
        val b = Bundle()
        b.putBoolean(FinishScreenActivity.ARG_YOU_WON, youWon)
        intent.putExtras(b)
        context.startActivity(intent)
    }
}