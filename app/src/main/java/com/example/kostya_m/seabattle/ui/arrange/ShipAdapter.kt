package com.example.kostya_m.seabattle.ui.arrange

import android.content.ClipData
import android.graphics.Color
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.data.model.local.Ship
import com.example.kostya_m.seabattle.data.model.local.ShipOrientation
import com.example.kostya_m.seabattle.di.ConfigPersistent
import com.example.kostya_m.seabattle.util.extension.inflate
import javax.inject.Inject

/**
 * Created by Kostya_M on 30.10.2016.
 */
@ConfigPersistent
class ShipAdapter @Inject constructor(): RecyclerView.Adapter<ShipAdapter.ShipViewHolder>() {
    //http://stackoverflow.com/questions/34586637/get-center-visible-item-of-recycleview-when-scrolling
    companion object {
        val VIEW_TYPE_PADDING = 1
        val VIEW_TYPE_ITEM = 2
    }

    var dragListener: DragShipListener? = null

    var ships: MutableList<Ship?>? = null
    set(value) {
        field = mutableListOf(null)
        value!!.forEach { field!!.add(it) }
        field!!.add(null)
        notifyDataSetChanged()
    }
    var paddingWidth = 0

    private var selectedItem = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShipViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            val holder = ShipViewHolder(parent)
            return holder
        } else {
            val holder = ShipViewHolder(parent)
            val lp = holder.itemView.layoutParams
            lp.width = paddingWidth
            holder.itemView.layoutParams = lp
            return holder
        }
    }

    override fun onBindViewHolder(holder: ShipViewHolder?, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            holder?.bind(ships!![position]!!)
            if (position == selectedItem) {
                holder?.imageView?.setOnTouchListener(TouchListener())
                holder?.imageView?.alpha = 1.0f
            } else {
                holder?.imageView?.setOnTouchListener(null)
                holder?.imageView?.alpha = 0.3f
            }
        }
    }

    override fun getItemCount(): Int = ships?.size ?: 0

    fun setSelectedItem(selectedItem: Int) {
        if (selectedItem == 0 && itemCount > 2) {
            this.selectedItem = 1
        } else if (selectedItem == itemCount-1) {
            this.selectedItem = itemCount - 2
        } else {
            this.selectedItem = selectedItem
        }
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (ships!![position] == null) VIEW_TYPE_PADDING else VIEW_TYPE_ITEM
    }

    fun removeItem(ship: Ship) {
        ships?.remove(ship)
        notifyDataSetChanged()
    }

    inner class ShipViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.arrange_ship_item)) {
        var imageView : ImageView = itemView.findViewById(R.id.shipImage) as ImageView
        fun bind(ship: Ship) {
            var res : Int = 0
            if (ship.orientation == ShipOrientation.VERTICAL) {
                when (ship.length) {
                    1 -> res = R.drawable.ship_one
                    2 -> res = R.drawable.ship_two
                    3 -> res = R.drawable.ship_three
                    4 -> res = R.drawable.ship_four
                    else -> R.mipmap.ic_launcher
                }
            } else {
                when (ship.length) {
                    1 -> res = R.drawable.ship_one_hor
                    2 -> res = R.drawable.ship_two_hor
                    3 -> res = R.drawable.ship_three_hor
                    4 -> res = R.drawable.ship_four_hor
                    else -> R.mipmap.ic_launcher
                }
            }
            imageView.tag = ship
            imageView.setImageResource(res)
        }
    }

    inner class TouchListener: View.OnTouchListener {
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            if (event.action == MotionEvent.ACTION_DOWN) {
                dragListener?.dragStarted()
                val clipData = ClipData.newPlainText("", "")
                val shadowBuilder = View.DragShadowBuilder(v)
                v.startDrag(clipData, shadowBuilder, v, 0)
                v.visibility = View.INVISIBLE
                return true
            } else {
                return false
            }
        }
    }

    fun getSelectedShip(): Ship? {
        return ships?.getOrNull(selectedItem)
    }

    fun addShips(ships: Array<Ship>) {
        for (ship in ships)
            this.ships?.add(itemCount - 1, ship)
    }

    interface DragShipListener {
        fun dragStarted()
        fun dragEnded()
    }

}