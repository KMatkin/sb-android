package com.example.kostya_m.seabattle.ui.base

import android.accounts.NetworkErrorException
import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.kostya_m.seabattle.R
import com.example.kostya_m.seabattle.SeaBattleApplication
import com.example.kostya_m.seabattle.di.components.ActivityComponent
import com.example.kostya_m.seabattle.di.components.ConfigPersistentComponent
import com.example.kostya_m.seabattle.di.components.DaggerConfigPersistentComponent
import com.example.kostya_m.seabattle.di.modules.ActivityModule
import com.example.kostya_m.seabattle.util.extension.getApplicationComponent
import com.example.kostya_m.seabattle.util.extension.showErrorToast
import com.example.kostya_m.seabattle.util.navigation.Navigator
import org.jetbrains.anko.toast
import timber.log.Timber
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.atomic.AtomicLong
import javax.inject.Inject

/**
 * Created by Kostya_M on 23.10.2016.
 */
open class BaseActivity : AppCompatActivity(), ShowError {
    companion object {
        @JvmStatic private val KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID"
        @JvmStatic private val NEXT_ID = AtomicLong(0)
        @JvmStatic
        val componentsMap = HashMap<Long, ConfigPersistentComponent>()
    }
    @Inject
    lateinit var navigator: Navigator
    protected var activityId: Long = 0
    lateinit var activityComponent: ActivityComponent
        get

    val sharedPreferences by lazy {
        getApplicationComponent().sharedPreferences()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityId = savedInstanceState?.getLong(KEY_ACTIVITY_ID) ?: NEXT_ID.andIncrement

        if (componentsMap[activityId] != null)
            Timber.i("Reusing ConfigPersistentComponent id=%d", activityId)

        val configPersistentComponent = componentsMap.getOrPut(activityId, {
            Timber.i("Creating new ConfigPersistentComponent id=%d", activityId)

            DaggerConfigPersistentComponent.builder()
                    .applicationComponent(getApplicationComponent())
                    .build()
        })

        activityComponent = configPersistentComponent.activityComponent(ActivityModule(this))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_ACTIVITY_ID, activityId)
    }

    override fun onDestroy() {
        if (!isChangingConfigurations) {
            Timber.i("Clearing ConfigPersistentComponent id=%d", activityId)
            componentsMap.remove(activityId)
        }
        super.onDestroy()
    }

    override fun showError(th: Throwable) {
        this.showErrorToast(th)
    }
}